package term

import (
	"fmt"
	"testing"
)

func TestParseTermResp(t *testing.T) {

	for i, params := range []struct {
		input  string
		output Position
		err    error
	}{
		{
			input:  "\x1b[6;2R",
			output: Position{6, 2},
		},
		{
			input:  "abc\x1b[6;2Rdef",
			output: Position{6, 2},
		},
		{
			input:  "\x1b[6;2Rdef",
			output: Position{6, 2},
		},
		{
			input:  "\x1b[6;2R",
			output: Position{6, 2},
		},
		{
			input:  "\x1b[6;2J",
			output: Position{6, 2},
			err:    fmt.Errorf("foobar"),
		},
	} {

		p, err := parseTermPosResp(params.input)

		if params.err == nil && err != nil {
			t.Errorf("(case %d) error parsing input: %s", i, err)
			continue
		}
		if params.err != nil && err == nil {
			t.Errorf("expected error, but received none on case %d", i)
			continue
		}
		if err == nil {
			if p.X != params.output.X {
				t.Errorf("(case %d) incorrect X: got %d, expected %d", i, p.X, params.output.X)
			}
			if p.Y != params.output.Y {
				t.Errorf("(case %d) incorrect Y: got %d, expected %d", i, p.Y, params.output.Y)
			}
		}
	}
}
