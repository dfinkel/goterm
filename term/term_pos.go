package term

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/dfinkel/goterm/termio"
)

// Esc is the escape character's ASCII value
const Esc byte = 0x1b

const escPrefix = string(Esc) + "["

// Position represents a column and line
type Position struct {
	X, Y int
}

func (p Position) String() string {
	return fmt.Sprintf("(%d, %d)", p.X, p.Y)
}

// GetTermPos returns the x, y position of the cursor
func GetTermPos() (Position, error) {
	t := termio.NewIOS()
	if err := t.EnableRaw(); err != nil {
		return Position{}, fmt.Errorf(
			"failed to enter raw mode: %s", err)
	}
	defer func() {
		if err := t.Restore(); err != nil {
			log.Printf("failed to restore canonical mode: %s", err)
		}
	}()

	fmt.Printf("%s6n", escPrefix)
	buf := make([]byte, 10)
	n, err := os.Stdin.Read(buf)
	if err != nil {
		return Position{}, err
	}
	input := string(buf[:n])
	if n < 6 {
		return Position{}, fmt.Errorf(
			"input too short: %q", input)
	}
	return parseTermPosResp(input)
}

func escSubstr(s string, terminators string) (string, error) {
	if len(s) < 6 {
		return "", fmt.Errorf("string too short (must be at least 6 characters): %q", s)
	}
	idx := strings.Index(s, escPrefix)
	if idx == -1 {
		return "", fmt.Errorf("prefix not found: %q", s)
	}
	s = s[idx+2:]

	tidx := strings.IndexAny(s, terminators)
	if tidx == -1 {
		return "", fmt.Errorf("terminator not found: %q", s)
	}

	s = s[:tidx+1]

	return s, nil
}

func parseTermPosResp(s string) (Position, error) {
	escstr, escErr := escSubstr(s, "R")
	if escErr != nil {
		return Position{}, escErr
	}
	// For now assume the last character is an "R", it'll be a later refactor
	// to handle this correctly later.
	escstr = escstr[:len(escstr)-1]

	parts := strings.SplitN(escstr, ";", 2)
	if len(parts) < 2 {
		return Position{}, fmt.Errorf(
			"input lacks a ';': %q", s)
	}

	p := Position{}

	var atoiErr error
	p.X, atoiErr = strconv.Atoi(parts[0])
	if atoiErr != nil {
		return Position{}, fmt.Errorf(
			"failed to parse first coordinate as an integer %q: %s",
			s, atoiErr)
	}

	if len(parts[1]) == 0 {
		return Position{}, fmt.Errorf("zero-length string after ';': %q", s)
	}

	ypart := parts[1]

	p.Y, atoiErr = strconv.Atoi(ypart)
	if atoiErr != nil {
		return Position{}, fmt.Errorf(
			"failed to parse second coordinate as an integer %q: %s",
			s, atoiErr)
	}
	return p, nil
}
