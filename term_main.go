package main

import (
	"fmt"
	"gitlab.com/dfinkel/goterm/term"
)

func main() {
	position, err := term.GetTermPos()
	if err != nil {
		fmt.Printf("failed to get position: %s\n",
			err)
		return
	}
	fmt.Printf("cursor at %s\n", position)
}
