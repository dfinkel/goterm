// +build freebsd darwin openbsd netbsd

package termio

import (
	"fmt"
	"os"

	"golang.org/x/sys/unix"
)

func (t *IOS) setattr(newTIOS *unix.Termios) error {
	err := unix.IoctlSetTermios(int(os.Stdout.Fd()), unix.TIOCSETA, newTIOS)
	if err != nil {
		return fmt.Errorf("set attrs %#v: %s",
			*newTIOS, err)
	}
	t.curtios = *newTIOS
	return nil
}

func getattr() (*unix.Termios, error) {
	return unix.IoctlGetTermios(int(os.Stdout.Fd()),
		unix.TIOCGETA)
}
