package termio

import (
	"log"

	"golang.org/x/sys/unix"
)

// IOS wraps functions to manipulate the terminal via termios ioctls
type IOS struct {
	curtios  unix.Termios
	origtios unix.Termios
}

// NewIOS returns a new term.IOS
func NewIOS() *IOS {
	tios, err := getattr()
	if err != nil {
		log.Printf("unexpected error getting termios: %s", err)
		return nil
	}
	t := IOS{
		curtios:  *tios,
		origtios: *tios,
	}

	return &t
}

// EnableRaw switches the terminal to raw mode.
func (t *IOS) EnableRaw() error {
	newTIOS := t.origtios

	newTIOS.Iflag &= ^(uint32(unix.IGNBRK) | unix.BRKINT | unix.PARMRK | unix.ISTRIP |
		unix.INLCR | unix.IGNCR | unix.ICRNL | unix.IXON)
	newTIOS.Oflag &= ^uint32(unix.OPOST)
	newTIOS.Lflag &= ^(uint32(unix.ECHO) | unix.ECHONL | unix.ICANON | unix.ISIG | unix.IEXTEN)
	newTIOS.Cflag &= ^(uint32(unix.CSIZE) | unix.PARENB)
	newTIOS.Cflag |= unix.CS8

	return t.setattr(&newTIOS)
}

// Restore switches the terminal to back to its original mode.
func (t *IOS) Restore() error {
	return t.setattr(&t.origtios)
}
